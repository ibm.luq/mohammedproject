$(function () {

    function aljumalCalc(text) {
        let allLettersArr = [];
        for (let i = 0; i < text.length; i++) {
            const letter = text[i];
            switch (letter) {
                case 'ا':
                case "إ":
                case "أ":
                case "ؤ":
                case "ئ":
                case "ى":
                case "ء":
                    allLettersArr.push(1);
                    break;
                case "ب":
                case "آ":
                    allLettersArr.push(2);
                    break;
                case "ج":
                    allLettersArr.push(3);
                    break;
                case "د":
                    allLettersArr.push(4);
                    break;
                case "ه":
                case "ة":
                    allLettersArr.push(5);
                    break;
                case "و":
                    allLettersArr.push(6);
                    break;
                case "ز":
                    allLettersArr.push(7);
                    break;
                case "ح":
                    allLettersArr.push(8);
                    break;
                case "ط":
                    allLettersArr.push(9);
                    break;
                case "ي":
                    allLettersArr.push(10);
                    break;
                case "ك":
                    allLettersArr.push(20);
                    break;
                case "ل":
                    allLettersArr.push(30);
                    break;
                case "م":
                    allLettersArr.push(40);
                    break;
                case "ن":
                    allLettersArr.push(50);
                    break;
                case "س":
                    allLettersArr.push(60);
                    break;
                case "ع":
                    allLettersArr.push(70);
                    break;
                case "ف":
                    allLettersArr.push(80);
                    break;
                case "ص":
                    allLettersArr.push(90);
                    break;
                case "ق":
                    allLettersArr.push(100);
                    break;
                case "ر":
                    allLettersArr.push(200);
                    break;
                case "ش":
                    allLettersArr.push(300);
                    break;
                case "ت":
                    allLettersArr.push(400);
                    break;
                case "ث":
                    allLettersArr.push(500);
                    break;
                case "خ":
                    allLettersArr.push(600);
                    break;
                case "ذ":
                    allLettersArr.push(700);
                    break;
                case "ض":
                    allLettersArr.push(800);
                    break;
                case "ظ":
                    allLettersArr.push(900);
                    break;
                case "غ":
                    allLettersArr.push(1000);
                    break;
                default:
                    console.error("الحرف غير مدعوم!");
                    allLettersArr.push(0);

            }
        }
        let jumal_name = '';
        let sum = 0;
        for (const object of allLettersArr) {
            sum += parseInt(object);
        }
        return sum;
    }

    let localeValue = "";
    let hostValue = location.host;

    if (window.location.pathname.includes("/en")) {
        localeValue = "en";
    } else if (window.location.pathname.includes("/ar")) {
        localeValue = "ar";
    }
    // here the button in auction bids table
    const auctionBtns = document.querySelectorAll(".acceptAuctionBtn");
    // check if the element not null
    if (auctionBtns !== null) {
        auctionBtns.forEach(function (item) {
            item.addEventListener('click', function (event) {
                const confmMsg = confirm("Do you want to accept this bid on this auction?");
                if (confmMsg) {
                    const dataAttrs = item.dataset;
                    const auctionID = dataAttrs['auctionId'];
                    let auctionAcceptRequest = fetch(`http://${hostValue}/${localeValue}/dashboard/auction/api/accept`, {
                            method: "POST",
                            headers: {
                                "Content-Type": "application/json;charset=utf-8",
                                "X-CSRFToken": getCookie('csrftoken')
                            },
                            body: JSON.stringify({
                                auctionID: auctionID
                            })
                        })
                        .then((response) => response.json())
                        .then((data) => {
                            // check if the status is true then reload the page
                            if (data['status'] == true) {
                                location.reload();
                            } else {
                                alert(data['msg']);
                            }
                        })
                        .catch((error) => console.error(error));
                }
            })
        });
    }


    // status btn
    const auctionStatusBtn = document.querySelector("#auctionDetailsStatusBtn");
    if (auctionStatusBtn !== null) {
        auctionStatusBtn.addEventListener('click', function (event) {
            const dataAttrs = auctionStatusBtn.dataset;
            const auctionID = dataAttrs['auctionId'];
            let auctionAcceptRequest = fetch(`http://${hostValue}/${localeValue}/dashboard/auction/api/save_status`, {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json;charset=utf-8",
                        "X-CSRFToken": getCookie('csrftoken')
                    },
                    body: JSON.stringify({
                        auctionID: auctionID
                    })
                })
                .then((response) => response.json())
                .then((data) => {
                    // check if the status is true then reload the page
                    if (data['status'] == true) {
                        location.reload();
                    } else {
                        alert(data['msg']);
                    }
                })
                .catch((error) => console.error(error));
        });
    }


    // here the button in auction table in list view
    const listAuctionBtns = document.querySelectorAll(".auctionListBtn");
    // check if the element not null
    if (listAuctionBtns !== null) {
        listAuctionBtns.forEach(function (item) {
            item.addEventListener('click', function (event) {
                const confmMsg = confirm("Do you want to delete this auction?");
                if (confmMsg) {
                    const dataAttrs = item.dataset;
                    const auctionID = dataAttrs['auctionId'];
                    let auctionAcceptRequest = fetch(`http://${hostValue}/${localeValue}/dashboard/auction/api/delete`, {
                            method: "POST",
                            headers: {
                                "Content-Type": "application/json;charset=utf-8",
                                "X-CSRFToken": getCookie('csrftoken')
                            },
                            body: JSON.stringify({
                                auctionID: auctionID
                            })
                        })
                        .then((response) => response.json())
                        .then((data) => {
                            // check if the status is true then reload the page
                            if (data['status'] == true) {
                                location.reload();
                            } else {
                                alert(data['msg']);
                            }
                        })
                        .catch((error) => console.error(error));
                }
            })
        });
    }

    // here the button in videos list views to delete videos
    const listVideosBtns = document.querySelectorAll(".videoDeleteBtn");
    // check if the element not null
    if (listVideosBtns !== null) {
        listVideosBtns.forEach(function (item) {
            item.addEventListener('click', function (event) {
                const confmMsg = confirm("Do you want to delete this Video?");
                if (confmMsg) {
                    const dataAttrs = item.dataset;
                    const videoId = dataAttrs['videoId'];
                    let auctionAcceptRequest = fetch(`http://${hostValue}/${localeValue}/dashboard/videos/api/delete`, {
                            method: "POST",
                            headers: {
                                "Content-Type": "application/json;charset=utf-8",
                                "X-CSRFToken": getCookie('csrftoken')
                            },
                            body: JSON.stringify({
                                videoId: videoId
                            })
                        })
                        .then((response) => response.json())
                        .then((data) => {
                            // check if the status is true then reload the page
                            if (data['status'] == true) {
                                location.reload();
                            } else {
                                alert(data['msg']);
                            }
                        })
                        .catch((error) => console.error(error));
                }
            })
        });
    }


    // add new name to study button
    const addNameBtn = document.querySelector("#newNameBtn");
    if (addNameBtn !== null) {
        addNameBtn.addEventListener("click", function (event) {
            let nameBlock = $("#namesToStudy").clone();
            $("#namesWrapper").append(nameBlock);
            const namesInputTotal = document.querySelectorAll('.name-input').length;
            document.querySelector(".standard-auction-total").innerText = namesInputTotal * 10;
        });
    }

    var standardEntityName = '';
    const standardAuctionLi = document.querySelectorAll(".standard-auction-li-item");
    if (standardAuctionLi != null) {
        standardAuctionLi.forEach(function (item) {
            item.addEventListener('click', function (event) {
                const entityNameAuction = document.querySelector("#entityNameAuction");
                standardEntityName = this.innerText;
                entityNameAuction.value = "";
                entityNameAuction.value = this.innerText;


            });
        });
    }

    const allNames = document.querySelectorAll('.name-input');
    if (allNames != null) {
        allNames.forEach(function (item) {
            item.addEventListener("input", function (event) {
                const standardTotal = document.querySelector(".standard-auction-total");
            });
        });
    }

    //names form
    const namesForm = document.querySelector("#namesForm");
    if (namesForm !== null) {
        namesForm.addEventListener("submit", function (event) {
            event.preventDefault();
            const allNames = document.querySelectorAll('.name-input');
            let allNamesStr = [];
            allNames.forEach(element => {
                allNamesStr.push(element.value);
            });
            const namesTotal = document.querySelector(".standard-auction-total").innerText;
            const confmMsg = confirm("هل تريد تأكيد اشتراكك بالمزاد؟");
            const extraNote = document.querySelector("#standard-extra-note");
            if (confmMsg) {
                let auctionAcceptRequest = fetch(`http://${hostValue}/${localeValue}/auctions/api/standard`, {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json;charset=utf-8",
                            "X-CSRFToken": getCookie('csrftoken')
                        },
                        body: JSON.stringify({
                            names: allNamesStr,
                            entityName: standardEntityName,
                            totalPrice: namesTotal,
                            extraNote: extraNote.value
                        })
                    })
                    .then((response) => response.json())
                    .then((data) => {
                        // check if the status is true then reload the page
                        if (data['status'] == true) {
                            location.reload();
                        } else {
                            alert(data['msg']);
                        }
                    })
                    .catch((error) => console.error(error));
            }

        });
    }

    function reverse(s) {
        return s.split("").reverse().join("");
    }

    // puzzle page
    const puzzleNumberInput = document.querySelector("#mainPuzzleNumber");
    if (puzzleNumberInput != null) {
        puzzleNumberInput.addEventListener("input", function (event) {
            const firstNumber = document.querySelector("#firstNumber");
            const reversedNumber = document.querySelector("#reversedNumber");
            const mainNumber2 = document.querySelector("#mainNumber2");
            const firstEqual = document.querySelector("#firstEqual");
            const reversedNumber2 = document.querySelector("#reversedNumber2");
            const finalResult = document.querySelector("#finalResult");
            const mainNumberValue = this.value;
            const mainReversedNumberValue = parseInt(reverse(mainNumberValue));
            let subResult = 0;
            if (mainNumberValue < mainReversedNumberValue) {
                firstNumber.value = mainReversedNumberValue;
                reversedNumber.value = mainNumberValue;
                subResult = parseInt(firstNumber.value) - parseInt(reversedNumber.value);
            } else {
                firstNumber.value = mainNumberValue;
                reversedNumber.value = mainReversedNumberValue;
                subResult = parseInt(mainNumberValue) - parseInt(mainReversedNumberValue);
            }
            firstEqual.value = subResult;
            mainNumber2.value = subResult;
            reversedNumber2.value = reverse(subResult.toString());
            reversedNumber2.value = parseInt(reversedNumber2.valuekeyup);
            let final = parseInt(mainNumber2.value) + parseInt(reversedNumber2.value);
            finalResult.value = final;

        });
    }

    var customEntityTypeName = '';
    const customAuctionLi = document.querySelectorAll(".custom-auckeyuption-li-item");
    if (customAuctionLi != null) {
        customAuctionLi.forEach(function (item) {
            item.addEventListener('click', function (event) {
                const entityNameAuction = document.querySelector("#entity_type");
                customEntityTypeName = this.innerText;
                entityNameAuction.value = "";
                entityNameAuction.value = this.innerText;


            });
        });
    }

    const alumalTextArea = document.querySelector("#aljumal_textarea");
    if (alumalTextArea !== null) {
        alumalTextArea.addEventListener("keyup", function (event) {
            const word = this.value;
            setTimeout(() => {
                const jumalValue = aljumalCalc(word);
                const jumalResults = document.querySelector("#aljumal-result");
                jumalResults.innerText = jumalValue;
            }, 100);
        });
    }



});