from django.urls import path, include
from .views import *

urlpatterns = [
    path("", UpdateHomeDetailsView.as_view(), name='dashboard-update-details'),
    path("user", UserDashboard.as_view(), name='dashboard-user'),
    path("messages/", include("member_messages.urls"), name="messages_urls"),
    path("receipts/", include("receipts.urls"), name="receipts_urls"),
    path("user/auction", UsersAuctionList.as_view(), name='dashboard-user-auctions'),
    path("user/auction/<int:pk>", UserAuctionDetails.as_view(), name='dashboard-user-auctions-details'),
    path("users", UsersListView.as_view(), name='dashboard-list-users'),
    path("users/<int:pk>", UserDetailView.as_view(), name='dashboard-detail-users'),
    path("verified", VerifiedUsersListView.as_view(), name='dashboard-verified-users'),
    path("steps", AddStepsView.as_view(), name='dashboard-add-steps'),
    path("entity", AddEntityView.as_view(), name='dashboard-add-entity'),
    path("percentages", AddPercentagesView.as_view(), name='dashboard-add-percentage'),
    path("videos/", include("videos.urls"), name='dashboard-videos-urls'),
    path('percentages/<int:pk>', UpdatePercentageView.as_view(), name='dashboard-update-percentage'),
    path("delete/steps/<int:id>", delete_step_view, name='dashboard-delete-step'),
    path("delete/entity/<int:id>", delete_entity_view, name='dashboard-delete-entity'),
    path("delete/percentage/<int:id>", delete_percentage_view, name='dashboard-delete-percentage'),
    path("auction/list", AuctionListView.as_view(), name='dashboard-auctions-list'),
    path("auction/details/<int:pk>", AuctionDetailsView.as_view(), name='dashboard-auctions-detail'),
    path("auction/api/accept", AcceptAuctionAPIView.as_view(), name='dashboard-auctions-api-view'),
    path("auction/api/save_status", SaveStatusAuctionAPIView.as_view(), name='dashboard-auctions-api-save-status'),
    path("auction/api/delete", DeleteAuctionAPIView.as_view(), name='dashboard-auctions-api-delete'),
]
