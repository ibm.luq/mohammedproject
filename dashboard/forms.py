from django import forms
from django.utils.translation import gettext_lazy

POSITIONS = (
    ("circle", "Circle"),
    ("progress_bar", "Progress Bar")
)


class ProjectStepsForm(forms.Form):
    step = forms.CharField(label=gettext_lazy("Step: "), required=True)


class EntityForm(forms.Form):
    entity = forms.CharField(label=gettext_lazy("Entity: "), required=True)


class PercentagesForm(forms.Form):
    position = forms.ChoiceField(choices=POSITIONS, label=gettext_lazy("Position"), required=False)
    label = forms.CharField(label=gettext_lazy("Label"), required=True,
                            widget=forms.TextInput(attrs={"placeholder": gettext_lazy("Label to display")}))
    percentage = forms.IntegerField(label=gettext_lazy("Percentage"), required=True, max_value=10, min_value=0)


class HomeDetailsForm(forms.Form):
    name = forms.CharField(label=gettext_lazy("Name: "), required=False)
    title = forms.CharField(label=gettext_lazy("Title: "), required=False)
    facebook = forms.URLField(label=gettext_lazy("Facebook"), required=False)
    twitter = forms.URLField(label=gettext_lazy("Twitter"), required=False)
    youtube = forms.URLField(label=gettext_lazy("Youtube"), required=False)
    snapchat = forms.URLField(label=gettext_lazy("Snapchat"), required=False)
    instagram = forms.URLField(label=gettext_lazy("Instagram"), required=False)
    details_list_items = forms.CharField(widget=forms.Textarea(), label=gettext_lazy("Details List Items"),
                                         required=False)
    age = forms.IntegerField(label=gettext_lazy("Age: "), required=True)
    country = forms.CharField(label=gettext_lazy("Country: "), required=True)
