from django.views.generic import TemplateView
from termcolor import cprint
from django.views.generic.detail import (DetailView)
from django.views.generic.list import (ListView)

from receipts.models import PaidMember
from .forms import *
from django.shortcuts import redirect, render
from django.contrib import messages
from .models import (ProjectSteps, Entity)
from django.utils.translation import gettext_lazy
from home.models import (HomePageDetails, HomePercentages)
from auctions.models import (Auction, AuctionBids)
from django.http import JsonResponse
from django.contrib.auth.mixins import (LoginRequiredMixin, UserPassesTestMixin)
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from rest_framework.views import APIView
from ipware import get_client_ip
from django.contrib.auth.hashers import make_password
from users.forms import RegisterForm
from users.models import Member
from django.contrib.auth import authenticate, login
from django.db.models import Max
from home.countries import (ALL_ENGLISH_COUNTRIES)


class UserDashboard(LoginRequiredMixin, TemplateView):
    login_url = reverse_lazy("login-view")

    def get(self, request, *args, **kwargs):
        context = dict()
        context['register_form'] = RegisterForm()
        context['user'] = request.user
        context['title'] = gettext_lazy("Update Profile")
        context['english_countries'] = ALL_ENGLISH_COUNTRIES
        return render(request, "dashboard/user/profile.html", context=context)

    def post(self, request, *args, **kwargs):
        context = dict()
        context['title'] = gettext_lazy("Update Profile")
        context['register_form'] = RegisterForm(request.POST)
        if context['register_form'].is_valid():
            ip_address = ''
            client_ip, is_routable = get_client_ip(request)
            if client_ip is None:
                ip_address = "Unable To get the ip address"
            else:
                # We got the client's IP address
                if is_routable:
                    # The client's IP address is publicly routable on the Internet
                    ip_address = client_ip
                else:
                    # The client's IP address is private
                    ip_address = "The client's IP address is private"
            password = context['register_form'].cleaned_data.get("password")
            password_confirmation = context['register_form'].cleaned_data.get("password_confirmation")
            if password_confirmation != password:
                messages.error(request, gettext_lazy('Password confirmation not match!'), 'danger')
                return render(request, 'users/auth/register.html', context=context)
            else:
                # pass
                member = Member.objects.filter(pk=request.user.pk).first()
                member.name = context['register_form'].data.get("name")
                member.password = make_password(context['register_form'].cleaned_data.get("password"))
                member.email = context['register_form'].cleaned_data.get("email")
                member.phone = context['register_form'].cleaned_data.get("phone")
                member.country = request.POST.get("country")
                member.save()
                email = context['register_form'].cleaned_data.get("email")
                password = context['register_form'].cleaned_data.get("password")
                auth_user = authenticate(request, email=email, password=password)
                if auth_user is not None:
                    login(request=request, user=auth_user)
                messages.success(request, gettext_lazy("Your profile updated successfully!"), 'success')
                return redirect("dashboard-user")
        else:
            return render(request, 'users/auth/register.html', context=context)


class UpdateHomeDetailsView(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    login_url = reverse_lazy("login-view")

    def test_func(self):
        if self.request.user.is_superuser:
            return True
        else:
            return False

    def get(self, request, *args, **kwargs):
        home_form = HomeDetailsForm()
        update_data = HomePageDetails.objects.first()
        return render(request, "dashboard/update.html", context={
            "home_form": home_form,
            "title": gettext_lazy("Update home details"),
            "update_data": update_data
        })

    def post(self, request):
        data = request.POST.copy()
        home_form = HomeDetailsForm(data)
        if home_form.is_valid():
            update_data = HomePageDetails.objects.first()
            update_data.name = home_form.data.get("name")
            update_data.title = home_form.data.get("title")
            update_data.age = home_form.data.get("age")
            update_data.country = home_form.data.get("country")
            update_data.facebook = home_form.data.get("facebook")
            update_data.twitter = home_form.data.get("twitter")
            update_data.youtube = home_form.data.get("youtube")
            update_data.instagram = home_form.data.get("instagram")
            update_data.snapchat = home_form.data.get("snapchat")
            update_data.details_list_items = home_form.data.get("details_list_items")
            update_data.save()
            messages.success(request, gettext_lazy('Update Successfully!'), "success")
            return redirect("dashboard-update-details")
        else:
            messages.error(request, home_form.errors, "danger")
            return redirect("dashboard-update-details")


class AddStepsView(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    login_url = reverse_lazy("login-view")

    def test_func(self):
        if self.request.user.is_superuser:
            return True
        else:
            return False

    def get(self, request, *args, **kwargs):
        steps_form = ProjectStepsForm()
        all_steps = ProjectSteps.objects.all()
        return render(request, "dashboard/add_steps.html",
                      context={"steps_form": steps_form, "all_steps": all_steps, "title": gettext_lazy('Steps')})

    def post(self, request):
        data = request.POST.copy()
        steps_form = ProjectStepsForm(data)
        if steps_form.is_valid():
            step_object = ProjectSteps(step=steps_form.data.get("step"))
            step_object.save()
            messages.success(request, gettext_lazy('Step added Successfully!'), "success")
            return redirect("dashboard-add-steps")


class AddEntityView(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    login_url = reverse_lazy("login-view")

    def test_func(self):
        if self.request.user.is_superuser:
            return True
        else:
            return False

    def get(self, request, *args, **kwargs):
        entity_form = EntityForm()
        all_entities = Entity.objects.all()
        return render(request, "dashboard/add_entity.html",
                      context={"entity_form": entity_form, "all_entities": all_entities})

    def post(self, request):
        data = request.POST.copy()
        entity_form = EntityForm(data)
        if entity_form.is_valid():
            entity_object = Entity(entity=entity_form.data.get("entity"))
            entity_object.save()
            messages.success(request, gettext_lazy('Entity added Successfully!'), "success")
            return redirect("dashboard-add-entity")


class UpdatePercentageView(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    model = HomePercentages
    template_name = "dashboard/update_percentages.html"
    login_url = reverse_lazy("login-view")

    def test_func(self):
        if self.request.user.is_superuser:
            return True
        else:
            return False

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['update_form'] = PercentagesForm()
        context['title'] = gettext_lazy("Update label")
        return context

    def post(self, request, *args, **kwargs):
        data = request.POST.copy()
        percentage_form = PercentagesForm(data)
        if percentage_form.is_valid():
            per_obj = HomePercentages.objects.filter(pk=kwargs.get("pk")).first()
            # per_obj.position = percentage_form.data.get("position")
            per_obj.label = percentage_form.data.get("label")
            per_obj.percentage_value = percentage_form.data.get("percentage")
            per_obj.save()
            messages.success(request, gettext_lazy("Label updated Successfully!"), "success")
            return redirect("dashboard-add-percentage")


class AddPercentagesView(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    login_url = reverse_lazy("login-view")

    def test_func(self):
        if self.request.user.is_superuser:
            return True
        else:
            return False

    def get(self, request, *args, **kwargs):
        percentage_form = PercentagesForm()
        all_perns = HomePercentages.objects.all()
        return render(request, "dashboard/percentages.html",
                      context={
                          "percentage_form": percentage_form,
                          "title": gettext_lazy("Update percentage"),
                          "all_perns": all_perns
                      })

    def post(self, request):
        data = request.POST.copy()
        percentage_form = PercentagesForm(data)
        all_perns = HomePercentages.objects.all()
        # check the length of the percentages if it is 8 ok complete the process
        if all_perns.count() == 8:
            if percentage_form.is_valid():
                entity_object = Entity(entity=percentage_form.data.get("entity"))
                entity_object.save()
                messages.success(request, gettext_lazy("Label added Successfully!"), "success")
                return redirect("dashboard-add-percentage")
        else:
            messages.success(request, gettext_lazy("The Maximum number for percentages is 8, you cant add more!"),
                             "danger")
            return redirect("dashboard-add-percentage")


class AuctionListView(LoginRequiredMixin, UserPassesTestMixin, ListView):
    model = Auction
    template_name = "auctions/admin/list.html"
    login_url = reverse_lazy("login-view")
    ordering = ['-created_date']

    def test_func(self):
        if self.request.user.is_superuser:
            return True
        else:
            return False

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = gettext_lazy("Auctions")
        return context


class VerifiedUsersListView(LoginRequiredMixin, UserPassesTestMixin, ListView):
    model = PaidMember
    template_name = "dashboard/admin_users/verified_users.html"
    login_url = reverse_lazy("login-view")
    ordering = ['-created_date']
    queryset = PaidMember.objects.filter(is_paid=True)

    def test_func(self):
        if self.request.user.is_superuser:
            return True
        else:
            return False

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = gettext_lazy("Verified Users")
        return context


class UsersListView(LoginRequiredMixin, UserPassesTestMixin, ListView):
    model = Member
    template_name = "dashboard/admin_users/users_list.html"
    login_url = reverse_lazy("login-view")
    ordering = ['-joined_date']

    def test_func(self):
        if self.request.user.is_superuser:
            return True
        else:
            return False

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = gettext_lazy("List of the users")
        return context


class UserDetailView(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    model = Member
    template_name = "dashboard/admin_users/user_details.html"
    login_url = reverse_lazy("login-view")
    ordering = ['-joined_date']

    def test_func(self):
        if self.request.user.is_superuser:
            return True
        else:
            return False

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = gettext_lazy("User detail")
        return context


class UsersAuctionList(LoginRequiredMixin, ListView):
    model = Auction
    template_name = "dashboard/user/auctions.html"
    login_url = reverse_lazy("login-view")

    ordering = ['-created_date']

    # def get_queryset(self):
    #     return Auction.objects.filter(member_id=self.request.user.pk)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = gettext_lazy("Auctions")
        return context


class UserAuctionDetails(LoginRequiredMixin, DetailView):
    model = Auction
    template_name = "dashboard/user/auction_details.html"
    login_url = reverse_lazy("login-view")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        auction_obj = kwargs.get("object")
        context['bids'] = auction_obj.auctions_auction.all().order_by('-current_price')
        context['max_bid'] = auction_obj.auctions_auction.all().aggregate(Max("current_price"))
        context['title'] = gettext_lazy("Auctions Details")
        return context

    def post(self, request, *args, **kwargs):
        cprint(request.POST, 'blue')
        auction_obj = Auction.objects.filter(pk=request.POST.get("auction_id")).first()
        auction_price = float(request.POST.get('auction_price'))
        if auction_price < auction_obj.min_price:
            messages.error(request, gettext_lazy('Your bid less than minimum price!'), "danger")
        else:
            auction_bid = AuctionBids()
            auction_bid.auction = auction_obj
            auction_bid.member = request.user
            auction_bid.current_price = float(request.POST.get('auction_price'))
            auction_bid.save()
            messages.success(request, gettext_lazy('Your Bid added successfully!'), "success")
        return redirect(request.get_full_path())


class AuctionDetailsView(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    model = Auction
    template_name = "auctions/admin/details.html"
    login_url = reverse_lazy("login-view")

    def test_func(self):
        if self.request.user.is_superuser:
            return True
        else:
            return False

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = gettext_lazy("Auctions Details")
        auction_obj = kwargs.get("object")
        context['max_bid'] = auction_obj.auctions_auction.all().aggregate(Max("current_price"))
        context['bids'] = AuctionBids.objects.filter(auction_id=kwargs.get("object")).order_by('-current_price')
        return context


class AcceptAuctionAPIView(APIView):

    def post(self, request, format=None):
        auction_id = request.data.get("auctionID")
        auction_obj = AuctionBids.objects.filter(pk=auction_id).first()
        auction_obj.status = True
        auction_obj.save()
        return JsonResponse(data={"msg": "update done", "status": True}, status=200)


class SaveStatusAuctionAPIView(APIView):

    def post(self, request, format=None):
        auction_id = request.data.get("auctionID")
        auction_obj = Auction.objects.filter(pk=auction_id).first()
        auction_obj.status = True
        auction_obj.save()
        return JsonResponse(data={"msg": "update done", "status": True}, status=200)


class DeleteAuctionAPIView(APIView):

    def post(self, request, format=None):
        auction_id = request.data.get("auctionID")
        auction_obj = Auction.objects.filter(pk=auction_id).first()
        auction_obj.delete()
        return JsonResponse(data={"msg": "delete done", "status": True}, status=200)


def delete_step_view(request, id):
    ProjectSteps.objects.filter(pk=id).delete()
    messages.success(request, gettext_lazy("Step deleted successfully!"), "success")
    return redirect("dashboard-add-steps")


def delete_entity_view(request, id):
    Entity.objects.filter(pk=id).delete()
    messages.success(request, gettext_lazy("Entity deleted successfully!"), "success")
    return redirect("dashboard-add-entity")


def delete_percentage_view(request, id):
    HomePercentages.objects.filter(pk=id).delete()
    messages.success(request, gettext_lazy("Percentage deleted successfully!"), "success")
    return redirect("dashboard-add-percentage")
