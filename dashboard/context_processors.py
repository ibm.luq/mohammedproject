from django.db.models import Q
from member_messages.models import (Message, Reply)
from users.models import Member


def get_unread_msgs_count(request):
    if request.user.is_authenticated:
        member = request.user
        admin = Member.objects.filter(email='mohammed@admin.com').first()
        msg_count = Message.objects.filter(member_id=member, is_seen=False).count()
        reply_count = Reply.objects.filter(member_id=member, is_seen=False).count()
        member_count_total = int(msg_count) + int(reply_count)
        admin_msg_count = Message.objects.filter(member_id=admin, is_seen=False).count()
        admin_reply_count = Reply.objects.filter(member_id=admin, is_seen=False).count()
        admin_total_count = int(admin_msg_count) + int(admin_reply_count)
        return {
            'msg_count': msg_count,
            "admin_total_count": admin_total_count,
            'member_count_total': member_count_total
        }
    else:
        return {
            'msg_count': 0,
            "admin_total_count": 0,
            'member_count_total': 0
        }
