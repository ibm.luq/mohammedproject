from django.db import models


class ProjectSteps(models.Model):
    class Meta:
        db_table = "project_step"

    step = models.CharField(max_length=70)
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.step


class Entity(models.Model):
    class Meta:
        db_table = "entity"

    entity = models.CharField(max_length=70)
    created_table = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.entity

