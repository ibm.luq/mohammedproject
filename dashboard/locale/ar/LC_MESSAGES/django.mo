��    c      4  �   L      p     q     �     �     �     �     �     �     �     �     �     �  	   �     �     	     	     !	     &	     ,	     :	     @	     R	     Y	     t	     �	     �	  	   �	     �	  	   �	  	   �	     �	     �	     �	      
  
   
     '
  	   9
     C
     Q
     ]
     o
     �
     �
     �
     �
     �
  
   �
  	   �
     �
     �
           '     8  
   I     T      e     �     �     �     �     �     �     �     �     �     �     �     
            ;     #   [          �  *   �     �     �  
   �                    !     3     B     W     k     x     �     �     �     �  "   �     �                 !   1  "   S     v  �  ~          1     G     ]  
   j  
   u     �     �     �     �  
   �  
   �     �            
   &  !   1  !   S     u     ~     �  (   �  $   �     �                )     A     V     r  *   �     �  *   �     �          2     B     R     d     �     �     �  
   �  2   �  !   
     ,     H     c     u  &   �     �     �     �     �  $   
     /     C     W     d  
   ~     �     �     �     �  *   �  $   �          (  
   7  J   B  9   �  0   �  9   �  @   2     s     �     �     �     �  
   �  "   �  "        6  (   V       (   �     �     �     �  !     7   )  1   a     �     �  ,   �  F   �  3   "     V     G   #       B           F   -          5       O      7   P   E   3   !           .   +             X   M             a           '       T               (   %      2                 @                  K          :   
          )       <   L   ,   >           H   ]   4   ?         "   *   $   9   8       &   R          N   c   J       [       	   I      1   W             b   0   S       V   C   /         ;   Z   `      Q   =   Y   \                      A   6   U                                 ^   D   _        Account Disabled Account Enabled Account Status Actions Add Age:  Auction Date Auction Name Auctions Auctions Details Country Country:  Current Price Delete Details List Items Edit Email Email Address Enter Enter The Auction Entity Entity added Successfully! Entity deleted successfully! Entity:  Facebook Full Name Initial Price Instagram Join Date Label Label added Successfully! Label to display Label updated Successfully! Last Login List of the users Max Price Maximum Price Member Name Member's Auctions Member's Messages Member's Receipts Minimum Price Name:  No bids on this auction yet Not Paid User Paid Price Paid User Password Password Confirmation Password confirmation not match! Payment Verified Payment verified Percentage Percentage Label Percentage deleted successfully! Phone Phone Number Position Receipt Date Send Send Message Snapchat Status Step Step added Successfully! Step deleted successfully! Step:  Steps Success The Maximum number for percentages is 8, you cant add more! The value should be between 1 to 10 This Member has no auction This Member has no messages This bid is holding until admin approve it Title:  Total Auctions Total Bids Total Messages Twitter Update Update My Profile Update Profile Update Successfully! Update home details Update label Update my personal details Update percentage User User detail User payment not verified User payment verified successfully Verified Users View View Auction Your Bid added successfully! Your bid less than minimum price! Your profile updated successfully! Youtube Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
 الحساب غير مفعل الحساب مفعل حالة الحساب خيارات اضافة العمر تاريخ المزاد اسم المزاد المزادات تفاصيل المزاد البلد البلد السعر الحالي حذف عناصر القائمة تعديل البريد الألكتروني البريد الألكتروني دخول انضم للمزاد الكيان:  تم اضافة الكيان بنجاح! تم حذف الكيان بنجاح! الكيان:  الفيسبوك الاسم الكامل السعر الأولي الأنستجرام تاريخ الإنضمام التسمية تم اضافة التسمية بنجاح! التسمية المعروضة تم تحديث التسمية بنجاح! آخر تسجيل دخول قائمة المستخدمين اقصى سعر اعلى سعر اسم العضو مزادات المستخدم رسائل المستخدم إيصالات المستخدم اقل سعر الأسم لا توجد مشاركات بهذا المزاد لم يتم تأكيد الدفع المبلغ المدفوع تم تأكيد الدفع كلمة السر تأكيد كلمة السر كلمة السر غير مطابقة! تأكيد الدفع تأكيد الدفع النسبة تسمية النسبة تم حذف الخطوة بنجاح! رقم الهاتف رقم الهاتف الموضع تاريخ الإيصال ارسال ارسال رسالة سناب شات الحالة الخطوة تم اضافة التسمية بنجاح! تم حذف الخطوة بنجاح! الخطوة:  الخطوات بنجاح اقصى عدد للنسب هي 8 لا يمكنك اضافة المزيد! القيمة الموضوعة تكون بين 1 الى 10 المستخدم لم يشترك بأي مزاد المستخدم لم يقم بأرسال اي رسالة هذه المشاركة بأنتظار موافقة المدير العنوان مجموع المزادات مجموع المراهنات مجموع الرسائل التويتر تحديث تحديث الملف الشخصي تحديث الملف الشخصي تم التحديث بنجاح! تحديث بيانات الرئيسية تعديل النسبة تحديث بياناتي الشخصية تحديث النسبة المستخدم بيانات المستخدم لم يتم تأكيد الدفع تم التأكد من عملية الدفع بنجاح الأعضاء المؤكدين على الدفع عرض عرض المزاد تم اضافة المزايدة بنجاح! المبلغ المدخل اقل من المبلغ المسموح به تم تحديث الملف الشخصي بنجاح! اليوتيوب 