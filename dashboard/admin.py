from django.contrib import admin
from .models import (Entity, ProjectSteps)
# Register your models here.

admin.site.register(Entity)
admin.site.register(ProjectSteps)
