from django.contrib import admin
from django.urls import path, include
from django.conf.urls.i18n import i18n_patterns
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('i18n/', include('django.conf.urls.i18n')),
]

urlpatterns += i18n_patterns(
    # path('admin/', admin.site.urls),
    path("", include("home.urls"), name='home-urls'),
    path("users/", include("users.urls"), name='users-urls'),
    path("auctions/", include("auctions.urls"), name='auctions-urls'),
    path("mohammed/", include("mohammed_ghailan_app.urls"), name='mohammed-urls'),
    path("dashboard/", include("dashboard.urls"), name='dashboard-urls'),
    prefix_default_language=False
)

# only in development
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
