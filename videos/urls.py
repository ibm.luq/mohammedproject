from django.urls import path
from .views import *

urlpatterns = [
    path("add", AddVideosViews.as_view(), name='videos-add'),
    path("list", ListVideosViews.as_view(), name='videos-list'),
    path("<int:pk>", DetailsVideosViews.as_view(), name='videos-details'),
    path("api/delete", DeleteVideoAPIView.as_view(), name='videos-api-delete'),
]
