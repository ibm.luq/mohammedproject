from django.shortcuts import render
from django.views.generic import (TemplateView, DetailView)
from termcolor import cprint
from django.core.files.storage import FileSystemStorage
from .forms import *
from django.utils.translation import gettext_lazy
from django.shortcuts import (redirect, render)
from django.contrib import messages
from .models import Videos
import os
from django.contrib.auth.mixins import (LoginRequiredMixin, UserPassesTestMixin)
from rest_framework.views import APIView
from django.urls import reverse_lazy
from django.core.files.storage import default_storage
from django.http import JsonResponse


class AddVideosViews(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    login_url = reverse_lazy("login-view")

    def test_func(self):
        if self.request.user.is_superuser:
            return True
        else:
            return False

    def get(self, request, *args, **kwargs):
        upload_video = AddVideoForm()
        return render(request, "videos/add.html", context={
            "upload_video": upload_video,
            "title": gettext_lazy("Upload new video"),
        })

    def post(self, request):
        upload_video_form = AddVideoForm(request.POST, request.FILES)
        if upload_video_form.is_valid():
            fs = FileSystemStorage()
            file_obj = request.FILES.get("video_thumbnail")
            filename = fs.save(os.path.join("thumbs", file_obj.name), file_obj)
            uploaded_file_url = fs.url(filename)
            fs2 = FileSystemStorage()
            file_obj2 = request.FILES.get("video_path")
            vidoename = fs.save(os.path.join("videos", file_obj2.name), file_obj2)
            upload_video_url = fs2.url(vidoename)

            vid_obj = Videos(title=upload_video_form.data.get("title"),
                             youtube_url=upload_video_form.data.get("youtube_url"),
                             video_thumbnail=uploaded_file_url, video_path=upload_video_url,
                             summary=upload_video_form.data.get("summary"),
                             body=upload_video_form.data.get("body"), member=request.user)

            vid_obj.save()
            messages.success(request, gettext_lazy("Video Upload Successfully!"), "success")
            return redirect("videos-add")
        else:
            messages.success(request, upload_video_form.errors, "danger")
            return redirect("videos-add")


class ListVideosViews(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    login_url = reverse_lazy("login-view")

    def test_func(self):
        if self.request.user.is_superuser:
            return True
        else:
            return False

    def get(self, request, *args, **kwargs):
        upload_video = AddVideoForm()
        all_videos = Videos.objects.all().order_by("-created_date")
        return render(request, "videos/list.html", context={
            "upload_video": upload_video,
            "title": gettext_lazy("All Videos"),
            "all_videos": all_videos
        })


class DeleteVideoAPIView(APIView):

    def post(self, request, format=None):
        video = request.data.get("videoId")
        vid_obj = Videos.objects.filter(pk=video).first()
        # cprint(vid_obj.video_thumbnail.name.replace("/media/", ''), 'green')
        vid_obj.video_thumbnail.storage.delete(vid_obj.video_thumbnail.name.replace("/media/", ''))
        if vid_obj.video_path:
            default_storage.delete(vid_obj.video_path.name.replace("/media/", ''))
        vid_obj.delete()
        return JsonResponse(data={"msg": "delete done", "status": True}, status=200)


class DetailsVideosViews(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    model = Videos
    template_name = "videos/edit.html"

    login_url = reverse_lazy("login-view")

    def test_func(self):
        if self.request.user.is_superuser:
            return True
        else:
            return False

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['video_form'] = AddVideoForm()
        context['title'] = "Update Video"
        return context

    def post(self, request, *args, **kwargs):
        upload_video_form = AddVideoForm(request.POST, request.FILES)
        if upload_video_form.is_valid():
            vid_obj = Videos.objects.filter(pk=kwargs.get("pk")).first()
            uploaded_url = ''
            fs = FileSystemStorage()
            file_obj = request.FILES.get("video_thumbnail", None)
            if file_obj is not None:
                filename = fs.save(os.path.join("thumbs", file_obj.name), file_obj)
                uploaded_url = fs.url(filename)
            else:
                uploaded_url = vid_obj.video_thumbnail
            vid_obj.title = upload_video_form.data.get("title")
            vid_obj.youtube_url = upload_video_form.data.get("youtube_url")
            vid_obj.summary = upload_video_form.data.get("summary")
            vid_obj.body = upload_video_form.data.get("body")
            vid_obj.video_thumbnail = uploaded_url
            vid_obj.save()
            messages.success(request, gettext_lazy("Video updated Successfully!"), "success")
            return redirect("videos-list")
        else:
            return redirect("videos-details", pk=kwargs.get("pk"))
