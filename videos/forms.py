from django import forms
from django.utils.translation import gettext_lazy


class AddVideoForm(forms.Form):
    title = forms.CharField(label=gettext_lazy("Title: "), required=True)
    youtube_url = forms.URLField(label=gettext_lazy("Youtube URL: "), required=True)
    video_thumbnail = forms.FileField(label=gettext_lazy("Thumbnail: "), required=False)
    summary = forms.CharField(label=gettext_lazy('Summary: '), required=True)
    body = forms.CharField(label=gettext_lazy("Video Description: "), widget=forms.Textarea(), required=False)
    video_path = forms.FileField(label=gettext_lazy("Video Path: "), required=False)
