from django.db import models
from users.models import Member


class Videos(models.Model):
    class Meta:
        db_table = 'videos'

    member = models.ForeignKey(to=Member, on_delete=models.CASCADE, related_name="member_videos", null=True)
    title = models.CharField(max_length=50, db_index=True)
    youtube_url = models.URLField(max_length=250, null=False, unique=True)
    video_thumbnail = models.ImageField(upload_to="media/thumbs")
    summary = models.CharField(max_length=250)
    body = models.TextField(null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    views = models.BigIntegerField(default=0)
    video_path = models.FileField(upload_to='media/videos', null=True, blank=True)

    def __str__(self):
        return self.title
