from django.http import JsonResponse
from django.shortcuts import render
from django.utils.translation import gettext_lazy
from django.views.generic import TemplateView
from django.views.generic.detail import DetailView
from rest_framework.views import APIView
from termcolor import cprint
from .forms import (AuctionForm)
from django.shortcuts import redirect
from django.contrib import messages
from dashboard.models import (ProjectSteps, Entity)
from .models import (Auction, AuctionBids)
from django.contrib.auth.mixins import (LoginRequiredMixin)
from django.urls import reverse_lazy
from dashboard.models import Entity


class CustomAuctionView(LoginRequiredMixin, TemplateView):
    login_url = reverse_lazy("login-view")

    def get(self, request, *args, **kwargs):
        auction_form = AuctionForm()
        all_steps = ProjectSteps.objects.all()
        all_entities = Entity.objects.all()
        return render(request, "auctions/custom.html", context={"auction_form": auction_form,
                                                                "all_steps": all_steps,
                                                                "all_entities": all_entities,
                                                                "title": gettext_lazy('Custom Auction')})

    def post(self, request, *args, **kwargs):
        data = request.POST.copy()
        auction_form = AuctionForm(data)
        if auction_form.is_valid():
            # dict_keys(['status', 'initial_price', 'min_price', 'max_price', 'counseling_goals', 'extra_notes', 'member_bio', 'entity_type', 'names_to_study'])
            entity_type = auction_form.cleaned_data.get("entity_type")
            entity_obj = Entity.objects.filter(entity=entity_type).first()
            user_obj = request.user
            total_price = float(auction_form.cleaned_data.get("initial_price"))
            auction = Auction()
            auction.auction_type = "Custom"
            auction.name = f"Auction for user {user_obj.name}"
            auction.entity_type = entity_obj
            auction.member = user_obj
            auction.names_to_study = gettext_lazy("No Names In this auction")
            auction.member_bio = auction_form.cleaned_data.get("member_bio")
            auction.min_price = 0.0
            auction.max_price = 0.0
            auction.initial_price = 0.0
            auction.total_price = total_price
            auction.extra_notes = auction_form.cleaned_data.get('extra_notes')
            auction.counseling_goals = auction_form.cleaned_data.get("counseling_goals")
            auction.save()
            # save auction bid
            auction_bid_obj = AuctionBids()
            auction_bid_obj.auction = auction
            auction_bid_obj.member = user_obj
            auction_bid_obj.current_price = total_price
            auction_bid_obj.save()
            messages.success(request, gettext_lazy('Your Auction added successfully!'), "success")
            return redirect('payment-view')
        else:
            messages.error(request, auction_form.errors, "danger")
            return render(request, "auctions/custom.html", context={"receipt_form": auction_form})


class StandardAuctionView(LoginRequiredMixin, TemplateView):
    login_url = reverse_lazy("login-view")

    def get(self, request, *args, **kwargs):
        all_entities = Entity.objects.all()
        return render(request, "auctions/standard.html",
                      context={"all_entities": all_entities, "title": gettext_lazy('Standard Auction')})


class StandardAuctionAPI(APIView):

    def post(self, request, *args, **kwargs):
        names = request.data.get("names")
        entity_type = request.data.get("entityName")
        total_price = request.data.get("totalPrice")
        user_obj = request.user
        entity_obj = Entity.objects.filter(entity=entity_type).first()
        names = ", ".join(names)
        extra_note = request.data.get("extraNote")
        auction = Auction()
        auction.name = f"Auction for user {user_obj.name}"
        auction.auction_type = "Standard"
        auction.total_price = float(total_price)
        auction.entity_type = entity_obj
        auction.member = user_obj
        auction.names_to_study = names
        auction.member_bio = ""
        auction.min_price = 0.0
        auction.max_price = 0.0
        auction.extra_notes = extra_note
        auction.counseling_goals = ''
        auction.initial_price = 0.0
        auction.save()
        # save auction bid
        auction_bid_obj = AuctionBids()
        auction_bid_obj.auction = auction
        auction_bid_obj.member = user_obj
        auction_bid_obj.current_price = float(total_price)
        auction_bid_obj.save()

        messages.success(request, gettext_lazy('Your Auction added successfully!'), "success")
        return JsonResponse(data={"msg": "added successfully", "status": True}, status=200)


class LossView(LoginRequiredMixin, TemplateView):
    template_name = "auctions/loss.html"
    login_url = reverse_lazy("login-view")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = gettext_lazy("You loss the auction")
        return context


class EnterAuction(LoginRequiredMixin, DetailView):
    template_name = "auctions/enter_auction.html"
    model = Auction
    login_url = reverse_lazy("login-view")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = gettext_lazy("Place your bid!")
        # context['object'] =
        return context
