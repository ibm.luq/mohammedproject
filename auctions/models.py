from django.db import models
from users.models import Member
from dashboard.models import Entity
from django.utils.translation import gettext_lazy

Auction_Choices = (
    ("Standard", "Standard"),
    ("Custom", "Custom"),
)


class Auction(models.Model):
    class Meta:
        db_table = 'auctions'

    def __str__(self):
        return self.name

    name = models.CharField(max_length=100, null=True, blank=True)
    status = models.BooleanField(default=False, db_index=True)
    auction_type = models.CharField(choices=Auction_Choices, blank=True, null=True, max_length=20)
    initial_price = models.DecimalField(max_digits=5, decimal_places=2)
    min_price = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    max_price = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    counseling_goals = models.TextField(null=True, blank=True)
    extra_notes = models.TextField(null=True, blank=True)
    member_bio = models.TextField(null=True, blank=True)
    entity_type = models.ForeignKey(Entity, on_delete=models.CASCADE, related_name="auction_entity_type", null=True)
    names_to_study = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)
    last_updated = models.DateTimeField(auto_now=True)
    member = models.ForeignKey(Member, on_delete=models.CASCADE, null=True, blank=True, related_name='member_auctions')
    total_price = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)

    @property
    def get_status_label(self):
        if self.status is True:
            return gettext_lazy("Open")
        else:
            return gettext_lazy("Closed")


class AuctionBids(models.Model):
    class Meta:
        db_table = "auctions_bids"

    status = models.BooleanField(default=True)
    member = models.ForeignKey(Member, on_delete=models.CASCADE, related_name="member_bids", null=True)
    auction = models.ForeignKey(Auction, related_name='auctions_auction', on_delete=models.CASCADE, null=True)
    current_price = models.DecimalField(max_digits=10, decimal_places=2)
    # bid_status = models.BooleanField(default=True, null=True, blank=True)

    def __str__(self):
        return f"Bid for {self.auction.name}"
