from django import forms
from django.utils.translation import gettext_lazy


class AuctionForm(forms.Form):
    status = forms.BooleanField(label=gettext_lazy("Status"), required=False)
    initial_price = forms.DecimalField(label=gettext_lazy("Initial Price"), required=True)
    min_price = forms.DecimalField(label=gettext_lazy("Minimum Price"), required=False)
    max_price = forms.DecimalField(label=gettext_lazy("Maximum Price"), required=False)
    counseling_goals = forms.CharField(label=gettext_lazy("Counseling Goals"), widget=forms.Textarea(), required=True)
    extra_notes = forms.CharField(label=gettext_lazy("Extra Notes"), widget=forms.Textarea(), required=False)
    member_bio = forms.CharField(label=gettext_lazy("Member Bio"), widget=forms.Textarea(), required=False)
    entity_type = forms.CharField(label=gettext_lazy("Entity Type"), required=True)
    total_price = forms.DecimalField(label=gettext_lazy("Total Price"), required=False)
    names_to_study = forms.CharField(label=gettext_lazy("Names to study"), widget=forms.Textarea(), required=False)
