from django.urls import path
from .views import *

urlpatterns = [
    path("custom", CustomAuctionView.as_view(), name='auctions-custom-view'),
    path("standard", StandardAuctionView.as_view(), name='auctions-standard-view'),
    path("api/standard", StandardAuctionAPI.as_view(), name='auctions-standard-api-view'),
    path("loss/<int:id>", LossView.as_view(), name='auctions-loss-view'),
    path("enter/<int:pk>", EnterAuction.as_view(), name='auctions-enter-view'),
]