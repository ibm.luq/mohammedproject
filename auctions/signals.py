from django.db.models.signals import post_save
from .models import AuctionBids
from termcolor import cprint


def check_the_status_if_bid_bigger_than_max_price(sender, instance, created, raw, using, update_fields, **kwargs):
    if created:
        # check if the paid price bigger than the max price
        if instance.current_price > instance.auction.max_price:
            instance.status = False
            instance.save()


post_save.connect(check_the_status_if_bid_bigger_than_max_price, sender=AuctionBids)
