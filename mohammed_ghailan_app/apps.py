from django.apps import AppConfig


class MohammedGhailanAppConfig(AppConfig):
    name = 'mohammed_ghailan_app'
