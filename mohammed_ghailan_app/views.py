import json
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from receipts.forms import (PaymentReceiptsForm)
from termcolor import cprint
from django.contrib import messages
from rest_framework.views import APIView
from django.http import JsonResponse
from home.models import HomePercentages


class FetchLabelAPIView(APIView):
    def post(self, request, format=None):
        lbl = request.data.get("labelCode", None)
        if lbl is not None:
            home_percn = HomePercentages.objects.filter(code=lbl).first()
            lbl_details = {
                "id": home_percn.pk,
                "position": home_percn.position,
                "label": home_percn.label,
                'code': home_percn.code,
                "value": home_percn.percentage_value
            }
            return JsonResponse(data={"data": lbl_details}, status=200)
        # try:
        #     pass
        #     return JsonResponse(data={'all_cats': cats_list}, status=200)
        # except Exception as ex:
        #     return JsonResponse(data={'msg': "there is error when get all categories"}, status=200)


class SendReceiptView(TemplateView):

    def get(self, request, *args, **kwargs):
        receipt_form = PaymentReceiptsForm()
        return render(request, "mohammed_ghailan_app/add_receipt.html", context={"receipt_form": receipt_form})

    def post(self, request):
        data = request.POST.copy()
        receipt_form = PaymentReceiptsForm(data)
        if receipt_form.is_valid():
            messages.success(request, "Send Successfully!", "success")
            return redirect("mohammed-send-receipt")
        else:
            messages.error(request, receipt_form.errors, "danger")
            return render(request, "mohammed_ghailan_app/add_receipt.html", context={"receipt_form": receipt_form})
