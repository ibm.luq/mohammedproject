from django.urls import path, include
from .views import *

urlpatterns = [
    path("receipt", SendReceiptView.as_view(), name='mohammed-send-receipt'),
    path("api/", include([
        path("fetch-label-value", FetchLabelAPIView.as_view(), name='api-fetch-label-value')
    ]), name='mohammed-api-url'),
]
