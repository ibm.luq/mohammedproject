from django.urls import path

from .views import *

urlpatterns = [
    path("admin", AdminMessagesList.as_view(), name='messages-admin-list'),
    path("admin/bookmark", AdminMessagesBookMarkView.as_view(), name='messages-admin-bookmark-list'),
    path("send", AdminSendMessageView.as_view(), name='messages-admin-send-message'),
    path("send/<int:pk>", AdminSendMessageView.as_view(), name='messages-admin-send-message'),
    path("admin/<int:pk>", AdminMessageDetail.as_view(), name='messages-admin-details'),
    path("admin/bookmakr/<int:pk>", AddMessageToBookMarkView.as_view(), name='message-admin-bookmark'),
    path("user", MemberMessagesList.as_view(), name='messages-user-list'),
    path("user/<int:pk>", MemberMessageDetail.as_view(), name='messages-user-details'),
]
