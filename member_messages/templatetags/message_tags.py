from django import template

register = template.Library()


@register.filter
def get_total_messages_and_replies(msgs, replies):
    return msgs + replies
