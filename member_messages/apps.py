from django.apps import AppConfig


class MemberMessagesConfig(AppConfig):
    name = 'member_messages'
