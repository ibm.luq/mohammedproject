from django.contrib import messages
from django.shortcuts import render
from django.views.generic import TemplateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.contrib.auth.mixins import (LoginRequiredMixin, UserPassesTestMixin)
from django.urls import reverse_lazy
from termcolor import cprint
from django.contrib.auth import get_user_model
from member_messages.models import (Message, Reply)
from users.models import Member
from django.utils.translation import gettext_lazy
from django.shortcuts import redirect


class AdminMessagesList(LoginRequiredMixin, UserPassesTestMixin, ListView):
    login_url = reverse_lazy("login-view")
    model = Message
    template_name = "member_messages/admin/list.html"
    ordering = ['-send_date']

    def test_func(self):
        if self.request.user.is_superuser:
            return True
        else:
            return False

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = gettext_lazy("Messages")
        return context


class AdminMessagesBookMarkView(LoginRequiredMixin, UserPassesTestMixin, ListView):
    login_url = reverse_lazy("login-view")
    model = Message
    template_name = "member_messages/admin/bookmark.html"
    queryset = Message.objects.filter(is_bookmark=True)

    def test_func(self):
        if self.request.user.is_superuser:
            return True
        else:
            return False

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = gettext_lazy("Bookmark")
        return context


class AdminSendMessageView(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    login_url = reverse_lazy("login-view")
    model = Message
    template_name = "member_messages/admin/send_message.html"

    def test_func(self):
        if self.request.user.is_superuser:
            return True
        else:
            return False

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'pk' in kwargs:
            context['member'] = Member.objects.get(pk=kwargs['pk'])

        else:
            context['members'] = Member.objects.all()
        context['title'] = gettext_lazy("Send Message")
        
        return context

    def post(self, request, *args, **kwargs):
        cprint(request.POST, 'green')
        messages.success(request, gettext_lazy("Message Sent successfully!"), "success")
        return redirect(request.get_full_path())


class AdminMessageDetail(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    login_url = reverse_lazy("login-view")
    model = Message
    template_name = "member_messages/admin/details.html"

    def test_func(self):
        if self.request.user.is_superuser:
            return True
        else:
            return False

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = gettext_lazy('Message Details')
        msg_obj = kwargs.get("object")
        context['all_replies'] = msg_obj.main_message.all().order_by("-reply_date")
        msg_obj.is_seen = True
        msg_obj.save()

        return context

    def post(self, request, *args, **kwargs):
        msg_obj = Message.objects.filter(pk=request.POST.get("message")).first()
        member_obj = Member.objects.filter(pk=request.POST.get("member")).first()
        reply = Reply()
        reply.title = request.POST.get("title")
        reply.message = msg_obj
        reply.member = member_obj
        reply.is_seen = False
        reply.msg = request.POST.get("reply")
        reply.sender = request.user
        reply.save()
        messages.success(request, gettext_lazy("Reply send successfully!"), 'success')
        return redirect(request.get_full_path())


class AddMessageToBookMarkView(LoginRequiredMixin, TemplateView):
    login_url = reverse_lazy("login-view")

    def get(self, request, *args, **kwargs):
        message_obj = Message.objects.filter(pk=kwargs.get("pk")).first()
        if message_obj.is_bookmark is True:
            message_obj.is_bookmark = False
            messages.success(request, gettext_lazy("Message Removed from bookmark successfully!"))
        else:
            message_obj.is_bookmark = True
            messages.success(request, gettext_lazy("Message Added to bookmark successfully!"))

        message_obj.save()
        # return redirect("messages-admin-list")

        return redirect("messages-admin-list")


class MemberMessagesList(LoginRequiredMixin, ListView):
    login_url = reverse_lazy("login-view")
    model = Message
    template_name = "member_messages/member/list.html"

    def get_queryset(self):
        return Message.objects.filter(member_id=self.request.user.pk)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Messages"
        return context


class MemberMessageDetail(LoginRequiredMixin, DetailView):
    login_url = reverse_lazy("login-view")
    model = Message
    template_name = "member_messages/member/details.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Message Details"
        msg_obj = kwargs.get("object")
        msg_obj.is_seen = True
        msg_obj.save()
        msg_obj = kwargs.get("object")
        context['all_replies'] = msg_obj.main_message.all().order_by("-reply_date")
        return context

    def post(self, request, *args, **kwargs):
        msg_obj = Message.objects.filter(pk=request.POST.get("message")).first()
        member_obj = Member.objects.filter(email="mohammed@admin.com").first()
        reply = Reply()
        reply.title = request.POST.get("title")
        reply.message = msg_obj
        reply.member = member_obj
        reply.is_seen = False
        reply.msg = request.POST.get("reply")
        reply.sender = request.user
        reply.save()
        messages.success(request, gettext_lazy("Reply send successfully!"), 'success')
        return redirect(request.get_full_path())
