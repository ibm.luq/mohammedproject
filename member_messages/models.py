from django.db import models
from users.models import Member
from django.utils.translation import gettext_lazy


# Create your models here.


class Message(models.Model):
    title = models.CharField(max_length=255, null=True, blank=True)
    member = models.ForeignKey(to=Member, on_delete=models.CASCADE, related_name="member_messages", null=True,
                               blank=True)
    msg = models.TextField()
    status = models.BooleanField(null=True, blank=True)
    is_seen = models.BooleanField(default=False)
    send_date = models.DateTimeField(auto_now_add=True)
    phone_number = models.CharField(null=True, blank=True, default="No Phone Number for the sender", max_length=50)
    is_bookmark = models.BooleanField(default=False, null=True, blank=True)

    @property
    def is_opened(self):
        if self.is_seen:
            return gettext_lazy("Message Seen")
        else:
            return gettext_lazy("Message Not Seen")

    def __str__(self):
        return f"Message {self.title}"


class Reply(models.Model):
    title = models.CharField(max_length=255, null=True, blank=True)
    message = models.ForeignKey(to=Message, on_delete=models.CASCADE, related_name="main_message")
    member = models.ForeignKey(to=Member, on_delete=models.CASCADE, related_name='reply_member')
    msg = models.TextField()
    is_seen = models.BooleanField(default=False)
    sender = models.ForeignKey(to=Member, on_delete=models.CASCADE, related_name="reply_sender")
    reply_date = models.DateTimeField(auto_now_add=True)

    @property
    def is_opened(self):
        if self.is_seen:
            return gettext_lazy("Message Seen")
        else:
            return gettext_lazy("Message Not Seen")

    def __str__(self):
        return f"Reply for Message {self.message.title}"
