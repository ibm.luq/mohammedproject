from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.generic import (TemplateView, DetailView)
from django.utils.translation import (gettext_lazy, get_language_info, get_language)
from ipware import get_client_ip
from home.models import (HomePercentages, HomePageDetails)
from termcolor import cprint
from videos.models import Videos
from receipts.forms import (PaymentReceiptsForm)
from receipts.models import (PaymentReceipts, PaidMember)
from django.core.files.storage import FileSystemStorage
import os
import uuid
from member_messages.models import Message


class HomeView(TemplateView):

    def get(self, request, *args, **kwargs):
        client_ip, is_routable = get_client_ip(request)
        all_pers = HomePercentages.objects.all()
        home_details = HomePageDetails.objects.first()
        details = home_details.details_list_items.split("\n")
        all_pers_list = dict()
        for per in all_pers:
            all_pers_list[per.code] = per.label
        
        return render(request, "home/home.html", context={
            "home_title": gettext_lazy("Home Page"),
            "all_pers": all_pers_list,
            "details": details,
            "all_info": home_details
        })


class ContactView(TemplateView):
    template_name = 'home/contact.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = gettext_lazy("Contact me")
        return context

    def post(self, request, *args, **kwargs):
        # ict_keys(['csrfmiddlewaretoken', 'name', 'email', 'phone', 'message', 'title])
        user = None
        # check if the user logged in
        if request.user.is_authenticated:
            user = request.user
        else:
            user = gettext_lazy('The Sender is not logged in')
        message_obj = Message()
        message_obj.member = user
        message_obj.title = request.POST.get("title")
        message_obj.msg = request.POST.get("message")
        message_obj.phone_number = request.POST.get("phone")
        message_obj.is_bookmark = False
        message_obj.status = False
        message_obj.save()
        messages.success(request, gettext_lazy("Your Message sent successfully!"), 'success')
        return redirect("contact-view")


class VideosView(TemplateView):

    def get(self, request, *args, **kwargs):
        all_videos = Videos.objects.all().order_by("-created_date")
        all_pers = HomePercentages.objects.all()
        home_details = HomePageDetails.objects.first()
        details = home_details.details_list_items.split("\n")
        all_pers_list = dict()
        for per in all_pers:
            all_pers_list[per.code] = per.label
        return render(request, "home/videos/list.html", context={
            "title": gettext_lazy("Videos"),
            "all_pers": all_pers_list,
            "details": details,
            "all_info": home_details,
            "all_videos": all_videos
        })


class VideoDetailsView(TemplateView):

    def get(self, request, *args, **kwargs):
        all_pers = HomePercentages.objects.all()
        home_details = HomePageDetails.objects.first()
        details = home_details.details_list_items.split("\n")
        all_pers_list = dict()
        for per in all_pers:
            all_pers_list[per.code] = per.label
        context = dict()
        vid_obj = Videos.objects.filter(pk=kwargs.get("pk")).first()
        vid_obj.views += 1
        vid_obj.save()
        context['object'] = vid_obj
        context['all_pers'] = all_pers_list
        context['all_info'] = home_details
        context['details'] = details
        context['title'] = vid_obj.title
        return render(request, "home/videos/details.html", context=context)


class AboutView(TemplateView):
    template_name = 'home/about.html'


class PrivacyPolicyView(TemplateView):
    template_name = 'home/privacy_policy.html'


class PricesView(TemplateView):
    template_name = 'home/prices.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = gettext_lazy("Prices")
        return context


class AljumalView(TemplateView):
    template_name = 'home/aljumal.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = gettext_lazy("Aljumal calculator")
        return context


class PuzzleView(TemplateView):
    template_name = 'home/puzzles.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = gettext_lazy("Puzzle")
        # context['object'] =
        return context


class PaymentView(TemplateView):
    template_name = 'home/payment.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = gettext_lazy("Payment profile")
        context['receipt_form'] = PaymentReceiptsForm()
        # context['object'] =
        return context

    def post(self, request, *args, **kwargs):
        data = request.POST.copy()
        receipt_form = PaymentReceiptsForm(request.POST, request.FILES)
        if receipt_form.is_valid():
            fs = FileSystemStorage()
            file_obj = request.FILES.get("receipt_img")
            full_file_name = uuid.uuid4().hex[:6].upper() + "_" + file_obj.name
            filename = fs.save(os.path.join("receipts", full_file_name), file_obj)
            uploaded_file_url = fs.url(filename)
            receipt = PaymentReceipts()
            receipt.member_id = request.user
            receipt.name = request.user.name
            receipt.email = request.user.email
            receipt.receipt_img = uploaded_file_url
            receipt.notes = receipt_form.cleaned_data.get("notes")
            receipt.save()
            paid_member = PaidMember()
            paid_member.member = request.user
            paid_member.receipt = receipt
            paid_member.is_paid = False
            paid_member.save()
            messages.success(request, gettext_lazy("Receipts Send Successfully!"), "success")
            return redirect(request.get_full_path())
        else:
            messages.error(request, receipt_form.errors, "danger")
            return render(request, "home/payment.html",
                          context={"receipt_form": receipt_form, 'title': gettext_lazy("Payment profile")})
