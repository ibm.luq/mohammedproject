"use strict";
$(function() {

  function aljumalCalc(text) {
    let allLettersArr = [];
    for (let i = 0; i < text.length; i++) {
      const letter = text[i];
      switch (letter) {
        case 'ا':
        case "إ":
        case "أ":
        case "ؤ":
        case "ئ":
        case "ء":
          allLettersArr.push(1);
          break;
        case "ب":
        case "آ":
          allLettersArr.push(2);
          break;
        case "ج":
          allLettersArr.push(3);
          break;
        case "د":
          allLettersArr.push(4);
          break;
        case "ه":
        case "ة":
          allLettersArr.push(5);
          break;
        case "و":
          allLettersArr.push(6);
          break;
        case "ز":
          allLettersArr.push(7);
          break;
        case "ح":
          allLettersArr.push(8);
          break;
        case "ط":
          allLettersArr.push(9);
          break;
        case "ى":
        case "ي":
          allLettersArr.push(10);
          break;
        case "ك":
          allLettersArr.push(20);
          break;
        case "ل":
          allLettersArr.push(30);
          break;
        case "م":
          allLettersArr.push(40);
          break;
        case "ن":
          allLettersArr.push(50);
          break;
        case "س":
          allLettersArr.push(60);
          break;
        case "ع":
          allLettersArr.push(70);
          break;
        case "ف":
          allLettersArr.push(80);
          break;
        case "ص":
          allLettersArr.push(90);
          break;
        case "ق":
          allLettersArr.push(100);
          break;
        case "ر":
          allLettersArr.push(200);
          break;
        case "ش":
          allLettersArr.push(300);
          break;
        case "ت":
          allLettersArr.push(400);
          break;
        case "ث":
          allLettersArr.push(500);
          break;
        case "خ":
          allLettersArr.push(600);
          break;
        case "ذ":
          allLettersArr.push(700);
          break;
        case "ض":
          allLettersArr.push(800);
          break;
        case "ظ":
          allLettersArr.push(900);
          break;
        case "غ":
          allLettersArr.push(1000);
          break;
        default:
          console.error("الحرف غير مدعوم!");
          allLettersArr.push(0);

      }
    }
    let jumal_name = '';
    let sum = 0;
    for (const object of allLettersArr) {
      sum += parseInt(object);
    }
    return sum;
  }

  let localeValue = "";
  const protocol = location.protocol;
  let hostValue = location.host;

  if (window.location.pathname.includes("/en")) {
    localeValue = "en";
  }
  // here the button in auction bids table
  const auctionBtns = document.querySelectorAll(".acceptAuctionBtn");
  // check if the element not null
  if (auctionBtns !== null) {
    auctionBtns.forEach(function(item) {
      item.addEventListener('click', function(event) {
        const confmMsg = confirm("Do you want to accept this bid on this auction?");
        if (confmMsg) {
          let reqUrl = '';
          if (localeValue === "") {
            reqUrl = `${protocol}//${hostValue}/dashboard/auction/api/accept`;
          } else {
            reqUrl = `${protocol}//${hostValue}/${localeValue}/dashboard/auction/api/accept`;
          }
          const dataAttrs = item.dataset;
          const auctionID = dataAttrs['auctionId'];
          let auctionAcceptRequest = fetch(reqUrl, {
              method: "POST",
              headers: {
                "Content-Type": "application/json;charset=utf-8",
                "X-CSRFToken": getCookie('csrftoken')
              },
              body: JSON.stringify({
                auctionID: auctionID
              })
            })
            .then((response) => response.json())
            .then((data) => {
              // check if the status is true then reload the page
              if (data['status'] == true) {
                location.reload();
              } else {
                alert(data['msg']);
              }
            })
            .catch((error) => console.error(error));
        }
      })
    });
  }


  // status btn
  const auctionStatusBtn = document.querySelector("#auctionDetailsStatusBtn");
  if (auctionStatusBtn !== null) {
    auctionStatusBtn.addEventListener('click', function(event) {
      const dataAttrs = auctionStatusBtn.dataset;
      const auctionID = dataAttrs['auctionId'];
      let reqUrl = '';
      if (localeValue === "") {
        reqUrl = `${protocol}//${hostValue}/dashboard/auction/api/save_status`;
      } else {
        reqUrl = `${protocol}//${hostValue}/${localeValue}/dashboard/auction/api/save_status`;
      }
      let auctionAcceptRequest = fetch(reqUrl, {
          method: "POST",
          headers: {
            "Content-Type": "application/json;charset=utf-8",
            "X-CSRFToken": getCookie('csrftoken')
          },
          body: JSON.stringify({
            auctionID: auctionID
          })
        })
        .then((response) => response.json())
        .then((data) => {
          // check if the status is true then reload the page
          if (data['status'] == true) {
            location.reload();
          } else {
            alert(data['msg']);
          }
        })
        .catch((error) => console.error(error));
    });
  }


  // here the button in auction table in list view
  const listAuctionBtns = document.querySelectorAll(".auctionListBtn");
  // check if the element not null
  if (listAuctionBtns !== null) {
    listAuctionBtns.forEach(function(item) {
      item.addEventListener('click', function(event) {
        const confmMsg = confirm("هل تريد حذف المزاد؟");
        if (confmMsg) {
          const dataAttrs = item.dataset;
          const auctionID = dataAttrs['auctionId'];
          let reqUrl = '';
          if (localeValue === "") {
            reqUrl = `${protocol}//${hostValue}/${localeValue}/dashboard/auction/api/delete`;
          } else {
            reqUrl = `${protocol}//${hostValue}/${localeValue}/dashboard/auction/api/delete`;
          }
          let auctionAcceptRequest = fetch(reqUrl, {
              method: "POST",
              headers: {
                "Content-Type": "application/json;charset=utf-8",
                "X-CSRFToken": getCookie('csrftoken')
              },
              body: JSON.stringify({
                auctionID: auctionID
              })
            })
            .then((response) => response.json())
            .then((data) => {
              // check if the status is true then reload the page
              if (data['status'] == true) {
                location.reload();
              } else {
                alert(data['msg']);
              }
            })
            .catch((error) => console.error(error));
        }
      })
    });
  }

  // here the button in videos list views to delete videos
  const listVideosBtns = document.querySelectorAll(".videoDeleteBtn");
  // check if the element not null
  if (listVideosBtns !== null) {
    listVideosBtns.forEach(function(item) {
      item.addEventListener('click', function(event) {
        const confmMsg = confirm("هل تريد حذف الفيديو؟");
        if (confmMsg) {
          const dataAttrs = item.dataset;
          const videoId = dataAttrs['videoId'];
          let reqUrl = '';
          if (localeValue === "") {
            reqUrl = `${protocol}//${hostValue}/dashboard/videos/api/delete`;
          } else {
            reqUrl = `${protocol}//${hostValue}/${localeValue}/dashboard/videos/api/delete`;
          }
          let auctionAcceptRequest = fetch(reqUrl, {
              method: "POST",
              headers: {
                "Content-Type": "application/json;charset=utf-8",
                "X-CSRFToken": getCookie('csrftoken')
              },
              body: JSON.stringify({
                videoId: videoId
              })
            })
            .then((response) => response.json())
            .then((data) => {
              // check if the status is true then reload the page
              if (data['status'] == true) {
                location.reload();
              } else {
                alert(data['msg']);
              }
            })
            .catch((error) => console.error(error));
        }
      })
    });
  }


  // add new name to study button
  const addNameBtn = document.querySelector("#newNameBtn");
  if (addNameBtn !== null) {
    addNameBtn.addEventListener("click", function(event) {
      let nameBlock = $("#namesToStudy").clone();
      $("#namesWrapper").append(nameBlock);
      console.log(nameBlock)
      const namesInputTotal = document.querySelectorAll('.name-input').length;
      document.querySelector(".standard-auction-total").innerText = namesInputTotal * 10;
    });
  }

  var standardEntityName = '';
  const standardAuctionLi = document.querySelectorAll(".standard-auction-li-item");
  if (standardAuctionLi != null) {
    standardAuctionLi.forEach(function(item) {
      item.addEventListener('click', function(event) {
        const entityNameAuction = document.querySelector("#entityNameAuction");
        standardEntityName = this.innerText;
        entityNameAuction.value = "";
        entityNameAuction.value = this.innerText;


      });
    });
  }

  const allNames = document.querySelectorAll('.name-input');
  if (allNames != null) {
    allNames.forEach(function(item) {
      item.addEventListener("input", function(event) {
        const standardTotal = document.querySelector(".standard-auction-total");
      });
    });
  }
  //names form
  const namesForm = document.querySelector("#namesForm");
  if (namesForm !== null) {
    namesForm.addEventListener("submit", function(event) {
      event.preventDefault();
      const allNames = document.querySelectorAll('.name-input');
      let allNamesStr = [];
      allNames.forEach(element => {
        allNamesStr.push(element.value);
      });
      const namesTotal = document.querySelector(".standard-auction-total").innerText;
      const confmMsg = confirm("هل تريد تأكيد اشتراكك بالمزاد؟");
      const extraNote = document.querySelector("#standard-extra-note");
      if (confmMsg) {
        let reqUrl = '';
        if (localeValue === "") {
          reqUrl = `${protocol}//${hostValue}/auctions/api/standard`;
        } else {
          reqUrl = `${protocol}//${hostValue}/${localeValue}/auctions/api/standard`;
        }
        let auctionAcceptRequest = fetch(reqUrl, {
            method: "POST",
            headers: {
              "Content-Type": "application/json;charset=utf-8",
              "X-CSRFToken": getCookie('csrftoken')
            },
            body: JSON.stringify({
              names: allNamesStr,
              entityName: standardEntityName,
              totalPrice: namesTotal,
              extraNote: extraNote.value
            })
          })
          .then((response) => response.json())
          .then((data) => {
            // check if the status is true then reload the page
            if (data['status'] == true) {
              location.href = location.origin + "/payment";
            } else {
              alert(data['msg']);
            }
          })
          .catch((error) => console.error(error));
      }

    });
  }

  function reverse(s) {
    return s.split("").reverse().join("");
  }

  // puzzle page
  //setup before functions
  let typingTimer; //timer identifier
  let doneTypingInterval = 1000; //time in ms (5 seconds)
  const puzzleNumberInput = document.querySelector("#mainPuzzleNumber");
  if (puzzleNumberInput != null) {
    //on keyup, start the countdown
    puzzleNumberInput.addEventListener('keyup', () => {
      // console.log('keyup')
      // clearTimeout(typingTimer);
      typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });

        //on keydown, clear the countdown
    puzzleNumberInput.addEventListener('keydown', () => {
      // console.log('keydown')
      clearTimeout(typingTimer);
    });

    //user is "finished typing," do something
    function doneTyping() {
      const firstNumber = document.querySelector("#firstNumber");
      const reversedNumber = document.querySelector("#reversedNumber");
      const mainNumber2 = document.querySelector("#mainNumber2");
      const firstEqual = document.querySelector("#firstEqual");
      const reversedNumber2 = document.querySelector("#reversedNumber2");
      const finalResult = document.querySelector("#finalResult");
      const mainNumberValue = puzzleNumberInput.value;
      const mainReversedNumberValue = reverse(mainNumberValue);
      // console.log('-->', isNaN(reversedNumber.value), parseInt(reversedNumber.value));
      let subResult = 0;
      if (mainNumberValue < mainReversedNumberValue) {
        firstNumber.value = mainReversedNumberValue;
        reversedNumber.value = mainNumberValue;

        // subResult = parseInt(firstNumber.value) - parseInt(reversedNumber.value);
        subResult = firstNumber.value - reversedNumber.value;
      } else {
        firstNumber.value = mainNumberValue;
        reversedNumber.value = mainReversedNumberValue;
        subResult = mainNumberValue - mainReversedNumberValue;
      }
      let subResultString = subResult.toString();
      // check if the firstNumber is 100
      if (firstNumber.value == 100) {
        firstEqual.value = "001";
      } else {
        firstEqual.value = subResult;
      }
      mainNumber2.value = subResult;
      reversedNumber2.value = parseInt(reverse(subResult.toString()));
      let final = parseInt(mainNumber2.value) + parseInt(reversedNumber2.value);
      finalResult.value = final;

      const puzzleSuccessAlert = document.querySelector("#puzzleSuccessAlert");
      const puzzleDangerAlert = document.querySelector("#puzzleDangerAlert");
    }

    setInterval(() => {
      const finalResult = document.querySelector("#finalResult");
      const firstNumber = document.querySelector("#firstNumber");

      // check if the number is not empty
      if (firstNumber.value !== "") {
        // check the final results if it is 1089
        if (finalResult.value == 1089) {
          puzzleSuccessAlert.style.display = "block";
          puzzleDangerAlert.style.display = "none";
        } else {
          puzzleSuccessAlert.style.display = 'none';
          puzzleDangerAlert.style.display = "block";
        }
      } else {
        puzzleSuccessAlert.style.display = "none";
        puzzleDangerAlert.style.display = "none";
      }

    }, 1000);

  }

  var customEntityTypeName = '';
  const customAuctionLi = document.querySelectorAll(".custom-auction-li-item");
  if (customAuctionLi != null) {
    customAuctionLi.forEach(function(item) {
      item.addEventListener('click', function(event) {
        const entityNameAuction = document.querySelector("#entity_type");
        customEntityTypeName = this.innerText;
        entityNameAuction.value = "";
        entityNameAuction.value = this.innerText;


      });
    });
  }

  const alumalTextArea = document.querySelector("#aljumal_textarea");
  if (alumalTextArea !== null) {
    alumalTextArea.addEventListener("keyup", function(event) {
      const word = this.value;
      setTimeout(() => {
        const jumalValue = aljumalCalc(word);
        const jumalResults = document.querySelector("#aljumal-result");
        jumalResults.innerText = jumalValue;
      }, 100);
    });
  }


  // here for custom auction
  const enterAuctionNumber = document.querySelector("#enterAuctionNumber");
  if (enterAuctionNumber !== null) {
    let auctionPrice;
    const auctionCurrentPriceRtl = document.querySelector("#auction-current-price-rtl");
    const auctionCurrentPrice = document.querySelector("#auction-current-price");
    if (auctionCurrentPrice !== null) {
      auctionPrice = auctionCurrentPrice;
    } else {
      auctionPrice = auctionCurrentPriceRtl;
    }
    enterAuctionNumber.addEventListener("keyup", (event) => {
      if (enterAuctionNumber.value == "") {
        auctionPrice.innerText = 0;
      } else {
        auctionPrice.innerText = '';
        auctionPrice.innerText = enterAuctionNumber.value;
      }

    });
  }


  // here to validate the percentage inputs
  $(".percentageInput").keydown(function(e) {
    // Save old value.

    if (!$(this).val() || (parseInt($(this).val()) <= 10 && parseInt($(this).val()) >= 0)) {
      $(this).data("old", 10);
    }

  });
  $(".percentageInput").keyup(function() {
    // Check correct, else revert back to old value.
    if (!$(this).val() || (parseInt($(this).val()) <= 10 && parseInt($(this).val()) >= 0))
    ;
    else
      $(this).val($(this).data("old"));
  });



});

function validateNumbers(evt) {
  var theEvent = evt || window.event;

  // Handle paste
  if (theEvent.type === 'paste') {
    key = event.clipboardData.getData('text/plain');
  } else {
    // Handle key press
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
  }
  var regex = /[0-9]|\./;
  if (!regex.test(key)) {
    theEvent.returnValue = false;
    if (theEvent.preventDefault) theEvent.preventDefault();
  }
}

function setLimit3Letters(element) {
  // console.log(element)
  const max_chars = 3;

  if (element.value.length > max_chars) {
    element.value = element.value.substr(0, max_chars);
  }
}
