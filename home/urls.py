from django.urls import path

from .views import *

urlpatterns = [
    path("", HomeView.as_view(), name='home-view'),
    path("contact", ContactView.as_view(), name='contact-view'),
    path("videos", VideosView.as_view(), name='videos-list-view'),
    path("videos/<int:pk>", VideoDetailsView.as_view(), name='videos-details-view'),
    path("about", AboutView.as_view(), name='about-view'),
    path("privacy", PrivacyPolicyView.as_view(), name='privacy-policy-view'),
    path("prices", PricesView.as_view(), name='prices-view'),
    path("puzzles", PuzzleView.as_view(), name='puzzles-view'),
    path("payment", PaymentView.as_view(), name='payment-view'),
    path("aljumal", AljumalView.as_view(), name='aljumal-view'),
]
