from django.contrib import admin
from .models import (HomePageDetails, HomePercentages)

admin.site.register(HomePageDetails)
admin.site.register(HomePercentages)
