# Generated by Django 3.1.7 on 2021-02-23 13:21

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='HomePageDetails',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=50, null=True)),
                ('title', models.CharField(blank=True, max_length=50, null=True)),
                ('status', models.BooleanField(default=True)),
                ('facebook', models.URLField(blank=True, max_length=250, null=True)),
                ('twitter', models.URLField(blank=True, max_length=250, null=True)),
                ('youtube', models.URLField(blank=True, max_length=250, null=True)),
                ('instagram', models.URLField(blank=True, max_length=250, null=True)),
                ('swift_bank_number', models.CharField(blank=True, max_length=50, null=True)),
                ('details_list_items', models.TextField()),
                ('age', models.IntegerField(blank=True, null=True)),
                ('country', models.CharField(blank=True, max_length=25, null=True)),
            ],
            options={
                'db_table': 'home_page_details',
            },
        ),
        migrations.CreateModel(
            name='HomePercentages',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('position', models.CharField(choices=[('circle', 'Circle'), ('progress_bar', 'Progress Bar')], max_length=50, null=True)),
                ('label', models.CharField(max_length=70)),
                ('code', models.CharField(blank=True, db_index=True, max_length=70, null=True, unique=True)),
                ('percentage_value', models.IntegerField()),
            ],
            options={
                'db_table': 'home_percentages',
            },
        ),
    ]
