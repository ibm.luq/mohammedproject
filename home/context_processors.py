from .models import (HomePercentages, HomePageDetails)


def get_all_home_data(request):
    all_pers = HomePercentages.objects.all()
    home_details = HomePageDetails.objects.first()
    details = home_details.details_list_items.split("\n")
    all_pers_list = dict()
    for per in all_pers:
        all_pers_list[per.code] = per.label

    context = {
        "all_pers": all_pers_list,
        "details": details,
        "all_info": home_details,
    }
    return context
