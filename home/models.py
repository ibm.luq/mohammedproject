from django.db import models

POSITIONS = (
    ("circle", "Circle"),
    ("progress_bar", "Progress Bar")
)


class HomePageDetails(models.Model):
    class Meta:
        db_table = 'home_page_details'

    name = models.CharField(max_length=50, null=True, blank=True)
    title = models.CharField(max_length=50, null=True, blank=True)
    status = models.BooleanField(default=True)
    facebook = models.URLField(max_length=250, null=True, blank=True)
    twitter = models.URLField(max_length=250, null=True, blank=True)
    youtube = models.URLField(max_length=250, null=True, blank=True)
    instagram = models.URLField(max_length=250, null=True, blank=True)
    snapchat = models.URLField(max_length=250, null=True, blank=True)
    swift_bank_number = models.CharField(max_length=50, null=True, blank=True)
    details_list_items = models.TextField()
    age = models.IntegerField(null=True, blank=True)
    country = models.CharField(max_length=25, blank=True, null=True)
    avatar = models.ImageField(upload_to="avatar/", null=True, blank=True)


class HomePercentages(models.Model):
    class Meta:
        db_table = 'home_percentages'
    position = models.CharField(max_length=50, choices=POSITIONS, null=True)
    label = models.CharField(max_length=70)
    code = models.CharField(max_length=70, null=True, blank=True, unique=True, db_index=True)
    percentage_value = models.IntegerField()

    def __str__(self):
        return self.code
