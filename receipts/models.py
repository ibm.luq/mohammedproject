from django.db import models
from django.utils.translation import gettext_lazy

from users.models import Member


class PaymentReceipts(models.Model):
    class Meta:
        db_table = "payment_receipts"

    member_id = models.ForeignKey(Member, on_delete=models.CASCADE, related_name="member_payment_receipts")
    name = models.CharField(max_length=50, blank=True, null=True)
    email = models.CharField(max_length=100, blank=True, null=True)
    receipt_img = models.ImageField(upload_to="receipts")
    status = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)
    notes = models.TextField(null=True, blank=True)


class PaidMember(models.Model):
    member = models.ForeignKey(to=Member, on_delete=models.CASCADE, related_name="paid_member")
    receipt = models.ForeignKey(to=PaymentReceipts, on_delete=models.CASCADE, related_name="paid_receipt")
    is_paid = models.BooleanField(default=False, null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)

    @property
    def is_paid_user(self):
        if self.is_paid is True:
            return gettext_lazy("Paid User")
        else:
            return gettext_lazy("Not Paid User")

