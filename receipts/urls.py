from django.urls import path

from .views import *

urlpatterns = [
    path("list", ReceiptsList.as_view(), name='receipts-list'),
    path("details/<int:pk>", ReceiptsDetails.as_view(), name='receipts-details'),
    path("verify/<int:pk>", VerifyMemberReceipt.as_view(), name='receipts-verify'),
]
