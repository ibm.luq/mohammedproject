from django import forms
from django.utils.translation import gettext_lazy


class PaymentReceiptsForm(forms.Form):
    name = forms.CharField(label=gettext_lazy("Name: "), required=False)
    email = forms.EmailField(label=gettext_lazy("Email: "), required=False)
    receipt_img = forms.FileField(label=gettext_lazy("Receipt Image: "), required=True)
    notes = forms.CharField(label=gettext_lazy("Extra notes: "), required=False, widget=forms.Textarea())
