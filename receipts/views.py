from django.contrib import messages
from django.shortcuts import render
from django.views.generic.base import View
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.contrib.auth.mixins import (LoginRequiredMixin, UserPassesTestMixin)
from termcolor import cprint

from .models import (PaymentReceipts, PaidMember)
from users.models import Member
from django.utils.translation import gettext_lazy
from django.shortcuts import redirect
from django.urls import reverse_lazy


class ReceiptsList(LoginRequiredMixin, UserPassesTestMixin, ListView):
    login_url = reverse_lazy("login-view")
    model = PaidMember
    ordering = ["-created_date"]
    template_name = "receipts/list.html"

    def test_func(self):
        if self.request.user.is_superuser:
            return True
        else:
            return False

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = gettext_lazy("Member Receipts")
        return context


class ReceiptsDetails(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    login_url = reverse_lazy("login-view")
    model = PaymentReceipts
    template_name = "receipts/details.html"

    def test_func(self):
        if self.request.user.is_superuser:
            return True
        else:
            return False

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = gettext_lazy("Member Receipt")
        payment_receipt_obj = kwargs.get('object')
        return context


class VerifyMemberReceipt(LoginRequiredMixin, UserPassesTestMixin, View):
    login_url = reverse_lazy("login-view")

    def test_func(self):
        if self.request.user.is_superuser:
            return True
        else:
            return False

    def get(self, request, *args, **kwargs):
        pk = int(kwargs.get('pk'))
        receipt_obj = PaymentReceipts.objects.filter(pk=pk).first()
        paid_member = receipt_obj.paid_receipt.all().filter(receipt=receipt_obj).first()
        paid_member.is_paid = True
        paid_member.save()
        messages.success(request, gettext_lazy("User has been verified successfully!"), "success")
        return redirect("receipts-details", pk=pk)
