from django.shortcuts import (render, redirect)
from django.views.generic import TemplateView
from django.contrib.auth import authenticate, login, logout
from termcolor import cprint
from .forms import (RegisterForm)
from .models import Member
from ipware import get_client_ip
from django.contrib import messages
from django.contrib.auth import logout
from django.contrib.auth.hashers import make_password
from django.utils.translation import gettext_lazy
from django.contrib.auth.mixins import (
    LoginRequiredMixin, UserPassesTestMixin)
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy, reverse
from home.countries import ALL_ENGLISH_COUNTRIES
from django.core.cache import cache
from django.http import HttpResponseRedirect


class LoginView(TemplateView):

    def get(self, request, *args, **kwargs):
        cache.set('next', request.GET.get('next', None))
        context = {'title': gettext_lazy("Login")}
        return render(request, 'users/auth/login.html', context=context)

    def post(self, request, *args, **kwargs):
        email = request.POST['email']
        password = request.POST['password']
        auth_user = authenticate(request, email=email, password=password)
        # cprint(auth_user, 'cyan')
        if auth_user is not None:
            login(request=request, user=auth_user)
            next_url = cache.get("next")
            if next_url:
                cache.delete('next')
                return HttpResponseRedirect(next_url)
            return redirect(reverse("home-view"))
        else:
            messages.error(request, gettext_lazy(
                "Email or Password not correct!"), 'danger')
            return render(request, 'users/auth/login.html', context={"title": gettext_lazy("Error Login!")})


class RegisterView(TemplateView):

    def get(self, request, *args, **kwargs):
        context = dict()
        context['register_form'] = RegisterForm()
        context['title'] = gettext_lazy("Register New User")
        context['english_countries'] = ALL_ENGLISH_COUNTRIES
        return render(request, 'users/auth/register.html', context=context)

    def post(self, request, *args, **kwargs):
        context = dict()
        context['register_form'] = RegisterForm(request.POST)
        if context['register_form'].is_valid():
            ip_address = ''
            client_ip, is_routable = get_client_ip(request)
            if client_ip is None:
                ip_address = "Unable To get the ip address"
            else:
                # We got the client's IP address
                if is_routable:
                    # The client's IP address is publicly routable on the Internet
                    ip_address = client_ip
                else:
                    # The client's IP address is private
                    ip_address = "The client's IP address is private"
            password = context['register_form'].cleaned_data.get("password")
            password_confirmation = context['register_form'].cleaned_data.get(
                "password_confirmation")
            if password_confirmation != password:
                messages.error(request, gettext_lazy(
                    'Password confirmation not match!'), 'danger')
                return render(request, 'users/auth/register.html', context=context)
            else:
                member = Member()
                member.name = context['register_form'].data.get("name")
                member.password = make_password(
                    context['register_form'].cleaned_data.get("password"))
                member.email = context['register_form'].cleaned_data.get(
                    "email")
                member.phone = context['register_form'].cleaned_data.get(
                    "phone")
                member.country = request.POST.get("country")
                member.is_active = True
                member.ip_address = ip_address
                member.save()
                email = context['register_form'].cleaned_data.get("email")
                password = context['register_form'].cleaned_data.get(
                    "password")
                auth_user = authenticate(
                    request, email=email, password=password)
                if auth_user is not None:
                    login(request=request, user=auth_user)

                messages.success(request, gettext_lazy(
                    "You registered successfully!"), 'success')
                return redirect("home-view")
        else:
            return render(request, 'users/auth/register.html', context=context)


class ProfileView(LoginRequiredMixin, TemplateView):
    template_name = 'users/profile.html'
    login_url = reverse_lazy("login-view")


def logout_view(request):
    logout(request)
    return redirect("home-view")
