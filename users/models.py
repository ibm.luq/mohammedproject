from django.db import models
from django.contrib.auth.models import (AbstractBaseUser, PermissionsMixin)
from .manager import CustomUserManager
from django.utils.translation import gettext_lazy

MEMBER_LABELS = (
    ("golden", gettext_lazy("Golden")),
    ("silver", gettext_lazy("Silver")),
    ("bronze", gettext_lazy("Bronze")),
)


class Member(AbstractBaseUser, PermissionsMixin):
    class Meta:
        db_table = "members"

    name = models.CharField(max_length=40, null=True, blank=True)
    email = models.CharField(max_length=200, db_index=True, unique=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    level = models.CharField(max_length=20, choices=MEMBER_LABELS, default=MEMBER_LABELS[2][0])
    phone = models.CharField(max_length=100, null=True, blank=True)
    status = models.BooleanField(default=True)
    country = models.CharField(max_length=50, blank=True, null=True)
    ip_address = models.CharField(max_length=100, blank=True, null=True)
    joined_date = models.DateTimeField(auto_now_add=True)

    objects = CustomUserManager()
    USERNAME_FIELD = "email"

    def __str__(self):
        return f"{self.name}"

