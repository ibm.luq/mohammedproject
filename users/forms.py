from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy
from .helpers import ALL_COUNTRIES
import re


def phone_number_validator(value):
    pattern = r"^\s*(?:\+?(\d{1,3}))?([-. (]*(\d{3})[-. )]*)?((\d{3})[-. ]*(\d{2,4})(?:[-.x ]*(\d+))?)\s*"
    rule = re.compile(pattern)

    if rule.search(value) is None:
        raise ValidationError(
            gettext_lazy('value is not a phone number'),
        )


class RegisterForm(forms.Form):
    name = forms.CharField(label=gettext_lazy("Full Name: "), required=False)
    # country = forms.ChoiceField(label=gettext_lazy("Country: "), choices=ALL_COUNTRIES, required=True)
    password = forms.CharField(label=gettext_lazy("Password: "), widget=forms.PasswordInput(), required=True)
    password_confirmation = forms.CharField(label=gettext_lazy("Password Confirmation: "), widget=forms.PasswordInput(),
                                       required=True)
    phone = forms.CharField(label=gettext_lazy("Phone Number: "), required=False, validators=[phone_number_validator])
    email = forms.EmailField(label=gettext_lazy("Email Address: "), required=True)
