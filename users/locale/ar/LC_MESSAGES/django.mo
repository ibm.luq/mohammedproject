��    3      �  G   L      h  
   i  	   t     ~     �     �     �     �     �     �  	   �     �     �  	                  =     C     J     S     _     u      �     �     �  $   �     �     �           	          *     1  "   9  &   \     �  O   �     �     �               "  8   )     b        0   �     �  0   �  A        `     h  �  �     
     .
     D
     U
     b
  !   x
  O   �
     �
  +         ,     D     ^     g      �  9   �     �     �               ,     I  *   f     �     �  K   �            
   5      @     a     |     �  (   �  ,   �  3   �  }   ,  &   �  "   �     �  %   	     /  �   D      �  *   �  g     G   �  e   �  ^   5     �  4   �               ,                                     %   /   3   *          0   $                             (   '       .          	                         
      )          #      2   &                 +          1   -      !                    "        Add Entity Add Steps Auctions Bronze Change password Email Address Email or Password not correct! Enter Password Error Login! Full Name Full Name:  Golden Home Page Home Percentages If you don't receive an email Login Logout Messages My Auctions Password Confirmation Password Confirmation:  Password confirmation not match! Phone Number Phone Number:  Please request a new password reset. Profile Receipts Register Register New User Reset Password Silver Success Superuser must have is_staff=True. Superuser must have is_superuser=True. The Email must be set The password reset link was invalid, possibly because it has already been used. Update my details Update my profile Users Verified Users Videos We've emailed you instructions for setting your password You registered successfully! You should receive them shortly. Your password has been set. You may go ahead and and check your spam folder. if an account exists with the email you entered. please make sure you've entered the address you registered with,  sign in value is not a phone number Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
 اضافة كيان اضافة خطوات المزادات برونزي كلمة المرور البريد الألكتروني البريد الألكتروني او كلمة المرور غير صحيحة! كلمة المرور حدث خطأ في تسجيل الدخول! الاسم الكامل الاسم الكامل:  ذهبي الصفحة الرئيسية النسب في الرئيسية إذا لم تتلق رسالة بريد إلكتروني تسجيل الدخول تسجيل خروج الرسائل المزادات تأكيد كلمة السر تأكيد كلمة السر كلمة المرور غير مطابقة! رقم الهاتف رقم الهاتف يرجى طلب إعادة تعيين كلمة المرور الجديدة. الملف الشخصي ايصلات الدفع تسجيل تسجيل مستخدم جديد نسيت كلمة السر فضي تم التسجيل بنجاح AR => Superuser must have is_staff=True. AR => Superuser must have is_superuser=True. يجب كتابة البريد الألكتروني رابط إعادة تعيين كلمة المرور غير صالح ، ربما لأنه تم استخدامه بالفعل. تحديث بياناتي الخاصة تحديث الملف الشخصي المستخدمين المستخدمين المؤكدين الفيديوهات لقد أرسلنا إليك عبر البريد الإلكتروني تعليمات حول تعيين كلمة المرور الخاصة بك تم الانضمام بنجاح يجب أن تستقبلهم قريبًا. تم تعيين كلمة المرور الخاصة بك. يمكنك المضي قدما و تستطيع وتحقق من مجلد الرسائل غير المرغوب فيها. إذا كان هناك حساب موجود بالبريد الإلكتروني الذي أدخلته. يرجى التأكد من إدخال العنوان الذي قمت بالتسجيل به ،  تسجيل الدخول القيمة المدخلة ليست رقم هاتف 