from django.urls import (path, include, reverse_lazy)
from .views import *
from django.contrib.auth import views as auth_views

urlpatterns = [
    path("login", LoginView.as_view(), name='login-view'),
    path("register", RegisterView.as_view(), name='register-view'),
    path("logout", logout_view, name='logout'),
    path("profile", ProfileView.as_view(), name='profile-view'),

    path("reset", auth_views.PasswordResetView.as_view(template_name='users/auth/reset.html'), name='reset_password'),
    path("reset/sent", auth_views.PasswordResetDoneView.as_view(template_name='users/auth/password_reset_done.html'),
         name='password_reset_done'),
    path('reset/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(template_name='users/auth/password_reset_confirm.html'), name='password_reset_confirm'),
    path("reset/complete",
         auth_views.PasswordResetCompleteView.as_view(template_name='users/auth/password_reset_complete.html'),
         name='password_reset_complete'),
]
